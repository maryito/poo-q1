from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, Integer, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///database/db')
Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()


class Producto(Base):
    __tablename__ = 'producto'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    cliente_id = Column(Integer, ForeignKey('cliente.id'))
    # Creamos la relación con cliente
    cliente = relationship('Cliente')


class Cliente(Base):
    __tablename__ = 'cliente'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    apellido = Column(String)
    telefono = Column(String)
    estado = Column(Boolean, default=True)
    # Creamos la relación con producto
    productos = relationship(Producto, backref="clientes")


Base.metadata.create_all(engine)

if __name__ == "__main__":
    # Instalación de las clases
    cliente1 = Cliente(nombre="Juan", apellido="Gonzales", telefono="1233333")
    # Creamos el producto y le pasamos al cliente que estamos creando
    producto1 = Producto(nombre="Manzana", cliente=cliente1)

    session.add_all([cliente1, producto1])
    session.commit()

    # consulta la informació
    query = session.query(Producto).all()

    # Recorrer el query de producto
    for x in query:
        print("ID: ", x.id, " Nombre: ", x.nombre, " Cliente: ", x.cliente_id)
