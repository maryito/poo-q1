from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, Integer, String

# Creo mi motor de base datos a utilizar
engine = create_engine('sqlite:///database/db')

# Base nos permite crear la relación de la clase con la tabla utilizando el ORM
Base = declarative_base()

# Se le pasa Base como herencia de la clase Contacto


class Contacto(Base):
    __tablename__ = 'contactos'
    # Nuestro atributos los creamos del tipo que necesitamos en nuestra tabla
    id = Column(Integer, primary_key=True)
    name = Column(String)


# A traves metadata creamos el modelo basado en clase en nuestra base de datos
# osea convierte nuestra clase en una tabla con sus atributos correspondiente
Base.metadata.create_all(engine)
