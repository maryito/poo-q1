# Clase #13 ORM

Para esta clase es necesario las siguientes paquetes de python.

```python
pip install sqlalchemy
```

El cuál se realiza el mapeo de objeto de una tabla llamada **Contacto** utilizando POO.

Ejemplos:

- [Contacto](./main.py)
- [Usuarios realizando consulta](./usuario.py)
- [Producto utilizando relaciones de base datos](./usuario.py)
