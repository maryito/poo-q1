from contacto import *
from sqlalchemy.orm import sessionmaker

if __name__ == "__main__":
    # Instancia de la clase Contacto
    nuevo = Contacto(name="Maryon Torres")

    print(nuevo.name)
    # Preparo la sessión para poder guardar la información en base de datos
    Session = sessionmaker(bind=engine)

    # Creo una nueva Instanaia
    session = Session()

    # Realizo el insert del modelo a base datos
    session.add(nuevo)

    # Debo siempre realizar commit sino no realiza cambios en base datos
    session.commit()

    # Creo una consulta a la base datos utilizando Modelo Contacto
    query = session.query(Contacto)

    # Solicito el primer elemento que tenga de tabla
    data = query.first()

    # Imprimo el Id de ese elemento utilizando el mapeo de la clase Contacto
    print(data.id)
