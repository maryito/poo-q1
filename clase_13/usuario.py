from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from sqlalchemy import Column, Integer, String

engine = create_engine('sqlite:///database/db')

Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()


class Usuario(Base):
    __tablename__ = 'usuarios'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    credencial = Column(String)


Base.metadata.create_all(engine)

if __name__ == "__main__":
    # Crear información de usuario
    pedro = Usuario(nombre='Pedro', credencial='12345678')
    maria = Usuario(nombre='Maria', credencial='12345678')

    # Guardamos la información del usuario #1
    session.add(pedro)

    # Guardamos la información del usuario #2
    session.add(maria)

    # Confirmamos los cambios
    session.commit()

    print("Resultado de la consulta es: ", pedro.credencial)
    print("El ID del usuario: ", maria.id)

    # Consultas utilizando filtro y atributos de la clase
    query = session.query(Usuario).filter_by(nombre='Pedro')
