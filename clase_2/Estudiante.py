class  Alumno:
    """ Clase Estudiante Representa aun Alumno en la UIP"""
    nombre = ""
    cedula = ""
    genero = ""
    carrera = ""
    estado = True
    def __init__(self,nom, ced):
        """ Inicializando los datos principales"""
        self.nombre = nom
        self.cedula = ced
    def imprimir(self):
        print("Su nombre es: ", self.nombre)
        print("Cedula: ",self.cedula )
        if self.carrera != "":
            print("Carrera: ", self.carrera)
        if self.genero != "":
            print("Genero: ", self.genero)
        if self.estado != "":
            print("Estado: ", self.estado)
        
    def solicitar(self):
        """Metodo para obtener la informacion del Estudiante"""
        print("--------------SOLICITANDO----------------")
        self.nombre = input("Ingrese el Nombre: ")
        self.cedula = input("Ingrese la Cedula: ")
        self.genero = input("Ingrese su Genero: ")
        self.estado = input("Ingrese su Estado: ")
        self.carrera = input("Ingrese su Carrera: ")
    def guardar(self):
        user = {
            "nombre": self.nombre,
            "cedula": self.cedula,
        }
        return user

"""
print("Sin utilizar MAIN")
maryon = Alumno("Maryon", "lkjlkjldj")
maryon.imprimir()
maryon.cedula = "XZkjdhkhf"
maryon.genero = "M"
maryon.carrera = "Ing. Sistemas"
maryon.imprimir()

# maryon.solicitar()
print("____________________________________________")
camila = Alumno("Camila", "123456")
camila.imprimir()
#camila.solicitar()
"""