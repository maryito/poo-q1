class Estudiante:
    nombre = ""
    cedula = ""
    __curso = []

    def __init__(self, _nombre, _cedula):
        self.nombre =  _nombre
        self.cedula = _cedula
    def imprimirDatos(self):
        return "Nombre: {0} Cedula: {1}".format(self.nombre, self.cedula)
    def __str__(self):
        return "Nombre: {0} Cedula: {1}".format(self.nombre, self.cedula)
    def getCurso(self):
        if len(self.__curso) == 0:
            print("Ho hay cursos matriculados...")
        else:
            for x in self.__curso:
                print(x)
    def setCurso(self, _materia):
        self.__curso.append(_materia)
        print("Se agrego un curso ")
